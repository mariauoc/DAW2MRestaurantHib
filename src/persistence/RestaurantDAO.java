/*
 * Clase encargada de la persistencia con Hibernate
 */
package persistence;

import entities.Cocinero;
import entities.Plato;
import excepctions.RestaurantException;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author mfontana
 */
public class RestaurantDAO {
    
    Session sesion;
    Transaction tx;

    public RestaurantDAO() {
        sesion = HibernateUtil.getSessionFactory().openSession();
    }
    
    public void insertarCocinero(Cocinero c) throws RestaurantException {
        Cocinero aux = getCocineroByNombre(c.getNombre());
        if (aux != null) {
            throw new RestaurantException("Ya existe un cocinero con ese nombre");
        }
        tx = sesion.beginTransaction();
        sesion.save(c);
        tx.commit();
    }
    
    public void modificarEspecialidadCocinero(Cocinero c, String nuevaEspecialidad) throws RestaurantException {
        // Necesitamos traer el objeto de la bbdd
        Cocinero aux = getCocineroByNombre(c.getNombre()) ;
        if (aux == null) {
            throw new RestaurantException("No existe el cocinero");
        }
        tx = sesion.beginTransaction();
        aux.setEspecialidad(nuevaEspecialidad);
        sesion.update(aux);
        tx.commit();
    }
    
    public Cocinero getCocineroByNombre(String nombre) {
        return (Cocinero) sesion.get(Cocinero.class, nombre);
    }
    
    public List<Plato> getPlatosByPostre() {
        Query q = sesion.createQuery("select p from Plato p where tipo='postre'");
        return q.list();
    }
    
    
    public void desconectar() {
        sesion.close();
        HibernateUtil.close();
    }
    
}
