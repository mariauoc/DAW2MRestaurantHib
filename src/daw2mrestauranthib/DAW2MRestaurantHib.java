/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daw2mrestauranthib;

import entities.Cocinero;
import entities.Plato;
import excepctions.RestaurantException;
import java.util.List;
import java.util.Set;
import persistence.RestaurantDAO;

/**
 *
 * @author mfontana
 */
public class DAW2MRestaurantHib {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Primer ejemplo con Hibernate");
        System.out.println("Estableciendo conexión...");
        RestaurantDAO restaurantDAO = new RestaurantDAO();
        System.out.println("Conexión establecida");
        Cocinero c = new Cocinero("DAW", "8", "H", 10, 10, "Postres");
        System.out.println("Dando de alta un cocinero...");
        try {
            restaurantDAO.insertarCocinero(c);
            System.out.println("Cocinero dado de alta");

        } catch (RestaurantException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Modificando especialidad de un cocinero");
        try {
            restaurantDAO.modificarEspecialidadCocinero(c, "Entrantes");
            c.setEspecialidad("Entrantes");
            System.out.println("Especialidad modificada");
        } catch (RestaurantException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Vamos a ver todos los platos que ha cocinado Karlos Arguiñano");
        Cocinero karlos = restaurantDAO.getCocineroByNombre("Karlos Arguiñano");
        Set<Plato> platos = (Set<Plato>) karlos.getPlatos();
        for (Plato p : platos) {
            System.out.println(p);
        }
        System.out.println("Listado de postres");
        List<Plato> ps = restaurantDAO.getPlatosByPostre();
        for (Plato p: ps) {
            System.out.println(p);
        }
        System.out.println("Cerrando sesión...");
        restaurantDAO.desconectar();
        System.out.println("Sesión cerrada.");
    }

}
